## Sources

### Instances

- Invidious<br>
_https://github.com/iv-org/documentation/blob/master/docs/instances.md_

- Nitter<br>
_https://github.com/zedeus/nitter/wiki/Instances_

- Bibliogram<br>
_https://git.sr.ht/~cadence/bibliogram-docs/tree/master/docs/Instances.md_

- Teddit<br>
_https://codeberg.org/teddit/teddit_

- Libreddit<br>
_https://github.com/spikecodes/libreddit_

- Scribe<br>
_https://git.sr.ht/~edwardloveall/scribe/tree/HEAD/docs/instances.md_

- Wikiless<br>
_https://codeberg.org/orenom/wikiless_

- ProxiTok<br>
_https://github.com/pablouser1/ProxiTok/wiki/Public-instances_


### Credits

- IP Geolocation by DB-IP
_https://db-ip.com_

- ip-location-db
_https://github.com/sapics/ip-location-db_


## CSV format

InstanceType, Domain


## Script connects to these addresses to download required files
- https://cdn.jsdelivr.net/npm/@ip-location-db/dbip-country/dbip-country-ipv4.csv
- https://cdn.jsdelivr.net/npm/@ip-location-db/dbip-country/dbip-country-ipv6.csv
- https://www.cloudflare.com/ips-v4
- https://www.cloudflare.com/ips-v6
