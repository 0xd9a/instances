import datetime
import os
import urllib.request
import ipaddress
from time import sleep
import itertools
import socket
import json


delay = 0.25

ip_data_v4_url = 'https://cdn.jsdelivr.net/npm/@ip-location-db/dbip-country/dbip-country-ipv4.csv'
ip_data_v6_url = 'https://cdn.jsdelivr.net/npm/@ip-location-db/dbip-country/dbip-country-ipv6.csv'

ip_data_v4_file = 'country-ipv4'
ip_data_v6_file = 'country-ipv6'

ip_data_v4 = []
ip_data_v6 = []

cf_ips_v4_file = 'cf-ips-v4'
cf_ips_v6_file = 'cf-ips-v6'

cf_ips_v4_url = 'https://www.cloudflare.com/ips-v4'
cf_ips_v6_url = 'https://www.cloudflare.com/ips-v6'

cf_ips_v4 = []
cf_ips_v6 = []

key_youtube = 'Youtube'
key_twitter = 'Twitter'
key_instagram = 'Instagram'
key_reddit = 'Reddit'
key_medium = 'Medium'
key_wikipedia = 'Wikipedia'
key_tiktok = 'TikTok'

alternatives = {
	key_youtube : 'lists/youtube.csv',
	key_twitter : 'lists/twitter.csv',
	key_instagram : 'lists/instagram.csv',
	key_reddit : 'lists/reddit.csv',
	key_medium : 'lists/medium.csv',
	key_wikipedia : 'lists/wikipedia.csv',
	key_tiktok : 'lists/tiktok.csv'
}

key_domain = 'domain'
key_type = 'type'
key_country = 'country'
key_cloudflare = 'cloudflare'

instances = {}


# Change user agent to prevent 403: Forbidden
opener = urllib.request.build_opener()
opener.addheaders = [('User-agent', 'Mozilla/5.0')]
urllib.request.install_opener(opener)


# Downlaod IP-to-Country data
print('Downloading IP-to-Country data...')
print('IPv4... ', end='')
urllib.request.urlretrieve(ip_data_v4_url, ip_data_v4_file)
print('Done')
sleep(delay)
print('IPv6... ', end='')
urllib.request.urlretrieve(ip_data_v6_url, ip_data_v6_file)
print('Done\n')
sleep(delay)


# Read IP-to-Country data
print('Reading IP-to-Country data...')
print('IPv4...', end='\r')
with open(ip_data_v4_file, 'r') as readfile:
	lines = readfile.readlines()
	for i, line in enumerate(lines):
		print(f'IPv4... {round(i/len(lines)*100)}%', end='\r')
		data = line.strip('\n').split(',')
		ip_start = ipaddress.ip_address(data[0])
		ip_end = ipaddress.ip_address(data[1])
		ip_range = ipaddress.summarize_address_range(ip_start, ip_end)
		ip_data_v4.append([ip_range, data[2]])
sleep(delay)
print('IPv4... Done')
sleep(delay)

print('IPv4...', end='\r')
with open(ip_data_v6_file, 'r') as readfile:
	lines = readfile.readlines()
	for i, line in enumerate(lines):
		print(f'IPv6... {round(i/len(lines)*100)}%', end='\r')
		data = line.strip('\n').split(',')
		ip_start = ipaddress.ip_address(data[0])
		ip_end = ipaddress.ip_address(data[1])
		ip_range = ipaddress.summarize_address_range(ip_start, ip_end)
		ip_data_v6.append([ip_range, data[2]])
sleep(delay)
print('IPv6... Done\n')
sleep(delay)


# Downlaod CloudFlare IP data
print('Downloading CloudFlare IP data...')
print('IPv4... ', end='')
urllib.request.urlretrieve(cf_ips_v4_url, cf_ips_v4_file)
print('Done')
print('IPv6... ', end='')
urllib.request.urlretrieve(cf_ips_v6_url, cf_ips_v6_file)
print('Done\n')


# Read CloudFlare IP data
print('Reading CloudFlare IP data...')
print('IPv4... ', end='\r')
with open(cf_ips_v4_file, 'r') as readfile:
	for line in readfile.readlines():
		cf_ips_v4.append(ipaddress.ip_network(line.strip('\n')))
print('IPv4... Done')
sleep(delay)
print('IPv6... ', end='\r')
with open(cf_ips_v6_file, 'r') as readfile:
	for line in readfile.readlines():
		cf_ips_v6.append(ipaddress.ip_network(line.strip('\n')))
print('IPv6... Done\n')
sleep(delay)



def isV4(ip): return type(ip) == ipaddress.IPv4Address

def ipInNetwork(ip, network):
	if (type(ip) == ipaddress.IPv4Address and type(network) == ipaddress.IPv4Network) or (type(ip) == ipaddress.IPv6Address and type(network) == ipaddress.IPv6Network):
		return network.supernet_of(ipaddress.ip_network(f'{ip}/{ip.max_prefixlen}'))
	return False

def isCloudFlare(ip):
	for subnet in cf_ips_v4 if isV4(ip) else cf_ips_v6:
		if ipInNetwork(ip, subnet):
			return True
	return False

def getCountry(ip):
	for item in ip_data_v4 if isV4(ip) else ip_data_v6:
		item[0], ip_ranges = itertools.tee(item[0])
		for ip_range in ip_ranges:
			if ipInNetwork(ip, ip_range):
				return item[1]
	return None


 
for service in alternatives:
	print(f'{service} redirections:\n')
	sleep(delay)
	instances[service] = []
	with open(alternatives[service], 'r') as readfile:
		for line in readfile.readlines():
			row = line.strip('\n').split(',')
			instance = {}
			instance[key_domain] = row[1]
			instance[key_type] = row[0]

			print(f'    {instance[key_domain]} ')
			ips = []
			try:
				ips = [ipaddress.ip_address(x[4][0]) for x in socket.getaddrinfo(instance[key_domain],443,type=socket.SOCK_STREAM)]
			except Exception as e:
				print('        Failed. Will not be included')
				print(f'        {e}')
				print()
				continue

			is_cloudflare = False

			for ip in ips:
				print(f'        {ip}', end='')
				if isCloudFlare(ip):
					is_cloudflare = True
				print(' | CloudFlare' if is_cloudflare else ' | OK')

			instance[key_cloudflare] = is_cloudflare

			countries = []
			if not is_cloudflare:
				for ip in ips:
					print('        ...', end='\r')
					country = getCountry(ip)
					if country and country not in countries: countries.append(country)
				if countries:
					if len(countries) > 1:
						print('        Countries: ', end='')
					else:
						print('        Country: ', end='')
					print(','.join(countries))
					instance[key_country] = countries
				else:
					print('        Country: Error')

			instances[service].append(instance)
			print()
			sleep(delay)

	print()
	sleep(delay)

	instances[service].sort(key=lambda item: item[key_cloudflare])


print('Writing final json... ', end='')
with open('instances.json', 'w') as writefile:
	writefile.writelines("{\n")
	for i, serviceName in enumerate(instances):
		serviceStartLine = f'"{serviceName}":[\n'
		writefile.writelines(serviceStartLine)
		service = instances[serviceName]
		for j, instance in enumerate(service):
			instanceLine = json.dumps(instance, separators=(',', ':'))
			if j != len(service)-1:
				instanceLine = instanceLine + ","
			instanceLine = instanceLine + "\n"
			writefile.writelines(instanceLine)
		serviceEndLine = "]"
		if i != len(instances)-1:
			serviceEndLine = serviceEndLine + ","
		writefile.writelines(serviceEndLine)
	writefile.writelines("\n}")
	
print('Done')
